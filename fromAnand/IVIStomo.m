function varargout = IVIStomo(varargin)
% IVISTOMO MATLAB code for IVIStomo.fig
%      IVISTOMO, by itself, creates a new IVISTOMO or raises the existing
%      singleton*.
%
%      H = IVISTOMO returns the handle to a new IVISTOMO or the handle to
%      the existing singleton*.
%
%      IVISTOMO('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in IVISTOMO.M with the given input arguments.
%
%      IVISTOMO('Property','Value',...) creates a new IVISTOMO or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before IVIStomo_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to IVIStomo_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help IVIStomo

% Last Modified by GUIDE v2.5 04-Mar-2018 16:32:19

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @IVIStomo_OpeningFcn, ...
                   'gui_OutputFcn',  @IVIStomo_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before IVIStomo is made visible.
function IVIStomo_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to IVIStomo (see VARARGIN)

% Choose default command line output for IVIStomo
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes IVIStomo wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = IVIStomo_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in selectDir.
function selectDir_Callback(hObject, eventdata, handles)
% hObject    handle to selectDir (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
dirpath = uigetdir;
[path,name,ext]=fileparts(dirpath);
handles.dataname = name(1:end - 4);
handles.dirpath = dirpath;
set(handles.dirpathtxt,'String', dirpath);
set(handles.datanametxt,'String', handles.dataname);
set(handles.showallsrc, 'Value', 0)
set(handles.showalldet, 'Value', 0)


D = dir(dirpath);
nsrc = 0;
n1 = length(handles.datanametxt);
for ii = 1:length(D)
    if ~isempty (strfind(D(ii).name, handles.dataname)) && (D(ii).isdir == 1)
        nsrc = nsrc + 1;
    end
end

set(handles.nsrctxt,'String',nsrc);
pwd1 = pwd;

cd(dirpath) 
cd([handles.dataname,'_001']);
axes(handles.optimage)
a = imread('luminescent.TIF');
imagesc(a);axis image;set(gca,'YDir', 'Normal')
colormap default

cd ..
for ii = 1:nsrc
    cd(sprintf([handles.dataname,'_%03d'],ii))
    text1 = fileread('ClickInfo.txt'); 
    a1 = strfind(text1, 'FL x-spot');
    srcpos(ii, 1) = str2num(text1(a1(1) + 11 : a1(1) + 17)); 
    a1 = strfind(text1, 'FL y-spot');
    srcpos(ii, 2) = str2num(text1(a1(1) + 11 : a1(1) + 17));
    cd ..    
end

text(srcpos(1,1),srcpos(1,2),'x','Color','r');
handles.srcpos = srcpos;
handles.nsrc = nsrc;

cd(pwd1)

%Load CT data
%need to add a flag to check if the CT folder exists. We may have optical tomo
%and use same CT for different tomo measurements
cd(dirpath);
cd('CT Data')
[CTvol CTmap] = dicomread([handles.dataname, '.dcm']);
CTvol=squeeze(CTvol);
nCTx = size(CTvol ,1);
handles.CTvol = CTvol;

set(handles.nCTvolx,'String', nCTx/2);%assuming nCTx is even for now

axes(handles.CTimage);
imagesc(squeeze(CTvol(nCTx/2, :, :)));
colormap(gray);axis image;set(gca,'YDir','Normal')
cd(pwd1)

guidata(hObject, handles)


function dirpathtxt_Callback(hObject, eventdata, handles)
% hObject    handle to dirpathtxt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of dirpathtxt as text
%        str2double(get(hObject,'String')) returns contents of dirpathtxt as a double


% --- Executes during object creation, after setting all properties.
function dirpathtxt_CreateFcn(hObject, eventdata, handles)
% hObject    handle to dirpathtxt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function currsrc_Callback(hObject, eventdata, handles)
% hObject    handle to currsrc (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of currsrc as text
%        str2double(get(hObject,'String')) returns contents of currsrc as a double

currsrc = str2num(get(hObject,'String'));
pwd1 = pwd;
cd(handles.dirpath) ;

if currsrc <= handles.nsrc & currsrc > 0
    cd(sprintf([handles.dataname,'_%03d'],currsrc));
    axes(handles.optimage);
    a = imread('luminescent.TIF');
    imagesc(a);axis image;set(gca,'YDir', 'Normal');colorbar
    text(handles.srcpos(currsrc,1), handles.srcpos(currsrc,2),'x','Color','r');
    set(handles.showallsrc, 'Value', 0)
    set(handles.showalldet, 'Value', 0)
else
    msgbox('Error: Source out of range')
end
cd(pwd1)


% --- Executes during object creation, after setting all properties.
function currsrc_CreateFcn(hObject, eventdata, handles)
% hObject    handle to currsrc (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function datanametxt_Callback(hObject, eventdata, handles)
% hObject    handle to datanametxt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of datanametxt as text
%        str2double(get(hObject,'String')) returns contents of datanametxt as a double


% --- Executes during object creation, after setting all properties.
function datanametxt_CreateFcn(hObject, eventdata, handles)
% hObject    handle to datanametxt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end




% --- Executes on button press in showallsrc.
function showallsrc_Callback(hObject, eventdata, handles)
% hObject    handle to showallsrc (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
axes(handles.optimage);

if get(hObject,'Value') == 1
    handles.showsrctxt = text(handles.srcpos(:,1), handles.srcpos(:,2),'x','Color','r');
else
    delete(handles.showsrctxt);
end
guidata(hObject, handles)


% --- Executes on button press in pushbutton3.
function pushbutton3_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)



function ndetX_Callback(hObject, eventdata, handles)
% hObject    handle to ndetX (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ndetX as text
%        str2double(get(hObject,'String')) returns contents of ndetX as a double


% --- Executes during object creation, after setting all properties.
function ndetX_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ndetX (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function ndetY_Callback(hObject, eventdata, handles)
% hObject    handle to ndetY (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ndetY as text
%        str2double(get(hObject,'String')) returns contents of ndetY as a double


% --- Executes during object creation, after setting all properties.
function ndetY_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ndetY (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function detStepX_Callback(hObject, eventdata, handles)
% hObject    handle to detStepX (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of detStepX as text
%        str2double(get(hObject,'String')) returns contents of detStepX as a double


% --- Executes during object creation, after setting all properties.
function detStepX_CreateFcn(hObject, eventdata, handles)
% hObject    handle to detStepX (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function detStepY_Callback(hObject, eventdata, handles)
% hObject    handle to detStepY (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of detStepY as text
%        str2double(get(hObject,'String')) returns contents of detStepY as a double


% --- Executes during object creation, after setting all properties.
function detStepY_CreateFcn(hObject, eventdata, handles)
% hObject    handle to detStepY (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton6.
function pushbutton6_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in genmeas.
function genmeas_Callback(hObject, eventdata, handles)
% hObject    handle to genmeas (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in gendet.
function gendet_Callback(hObject, eventdata, handles)
% hObject    handle to gendet (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
axes(handles.optimage);
temptxt = text(10,220,'Select corners detector ROI', 'Color', 'w','FontSize',12,'FontWeight','Bold');

[xx yy] = ginput(2);
[xx I] = sort(xx);
yy = yy(I);

delete(temptxt);

if isfield(handles, 'showdettxt' )
    delete(handles.showdettxt);
end
nx = str2num(get(handles.ndetX,'String'));
ny = str2num(get(handles.ndetY,'String'));

dx = str2num(get(handles.detStepX,'String'));
dy = str2num(get(handles.detStepY,'String'));

xx = linspace(xx(1), xx(2), nx);
detpos = [];
for ii = 1:nx
    detpos = [detpos; [xx(ii)*ones(ny, 1), linspace(yy(1), yy(2), ny)']];
end
%showalldet_Callback(hObject, eventdata, handles)
handles.showdettxt = text(detpos(:,1), detpos(:,2),'o','Color','y');
set(handles.showalldet, 'Value', 1)
handles.detpos = detpos;
guidata(hObject, handles);
function nCTvolx_Callback(hObject, eventdata, handles)
% hObject    handle to nCTvolx (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of nCTvolx as text
%        str2double(get(hObject,'String')) returns contents of nCTvolx as a double
xslice = str2num(get(hObject,'String'));%assuming nCTx is even for now

axes(handles.CTimage);
imagesc(squeeze(handles.CTvol(xslice, :, :)));
axis image;set(gca,'YDir','Normal')

% --- Executes during object creation, after setting all properties.
function nCTvolx_CreateFcn(hObject, eventdata, handles)
% hObject    handle to nCTvolx (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function nCTvolz_Callback(hObject, eventdata, handles)
% hObject    handle to nCTvolz (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of nCTvolz as text
%        str2double(get(hObject,'String')) returns contents of nCTvolz as a double


% --- Executes during object creation, after setting all properties.
function nCTvolz_CreateFcn(hObject, eventdata, handles)
% hObject    handle to nCTvolz (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function nCTvoly_Callback(hObject, eventdata, handles)
% hObject    handle to nCTvoly (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of nCTvoly as text
%        str2double(get(hObject,'String')) returns contents of nCTvoly as a double


% --- Executes during object creation, after setting all properties.
function nCTvoly_CreateFcn(hObject, eventdata, handles)
% hObject    handle to nCTvoly (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton11.
function pushbutton11_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton11 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)



function bin_Callback(hObject, eventdata, handles)
% hObject    handle to bin (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of bin as text
%        str2double(get(hObject,'String')) returns contents of bin as a double


% --- Executes during object creation, after setting all properties.
function bin_CreateFcn(hObject, eventdata, handles)
% hObject    handle to bin (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in showalldet.
function showalldet_Callback(hObject, eventdata, handles)
% hObject    handle to showalldet (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of showalldet
axes(handles.optimage);
if isfield(handles, 'showdettxt' )
    delete(handles.showdettxt);
end
if get(hObject,'Value') == 1
    if isfield(handles, 'detpos' )
        handles.showdettxt = text(handles.detpos(:,1), handles.detpos(:,2),'o','Color','y');
    end
else
    if isfield(handles, 'detpos' )
       delete(handles.showdettxt);
    end
end
guidata(hObject, handles)
