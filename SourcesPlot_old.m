%% Data name & Paths
path_this = pwd;
addpath(path_this);  

av_data = ls;

for i=1:size(av_data,1)
    if av_data(i,1:3) =='PKI'
        fi = i;
    end
end

data_name = av_data(fi,:);

%% Count sources
path_this_2 = strcat(path_this,'\');
path_this_2 = strcat(path_this_2,data_name);
cd(path_this_2);
av_data_2 = ls;
cont = 0;
for i=1:size(av_data_2,1)
    if mean(av_data_2(i,1:3) =='PKI') && mean(av_data_2(i,end-3:end) ~='.PNG')
        cont = cont+1;
    end
end

%% Retrieve X & Y source positions
string2 = path_this_2;
clear x y
for i = 1:cont
    
    if i<10
        data_name_i = data_name;
        data_name_i(end-2) = '0';
        data_name_i(end-1) = '0';
        data_name_i(end) = num2str(i);
    
        string3 = strcat(string2, '\');
        cd(strcat(string3, data_name_i));
        
        fid  = fopen('ClickInfo.txt','r');
        text = textscan(fid,'%s','Delimiter','','endofline','');
        text = text{1}{1};
        fid  = fclose(fid);
        idx_x = strfind(text,'FL x-spot:');
        x(i) = round(str2num(text(min(idx_x)+11:min(idx_x)+18)));
        idx_y = strfind(text,'FL y-spot:');
        y(i) = round(str2num(text(min(idx_y)+11:min(idx_y)+18)));
    end

    if i>=10
        ii=num2str(i);
        data_name_i = data_name;
        data_name_i(end-2) = '0';
        data_name_i(end-1) = ii(1);
        data_name_i(end) = ii(2);
    
        string3 = strcat(string2, '\');
        cd(strcat(string3, data_name_i));
        
        fid  = fopen('ClickInfo.txt','r');
        text = textscan(fid,'%s','Delimiter','','endofline','');
        text = text{1}{1};
        fid  = fclose(fid);
        idx_x = strfind(text,'FL x-spot:');
        x(i) = round(str2num(text(min(idx_x)+11:min(idx_x)+18)));
        idx_y = strfind(text,'FL y-spot:');
        y(i) = round(str2num(text(min(idx_y)+11:min(idx_y)+18)));
    end
    
end

for i = 1:cont
    
    if i<10
        data_name_i = data_name;
        data_name_i(end-2) = '0';
        data_name_i(end-1) = '0';
        data_name_i(end) = num2str(i);
    
        string3 = strcat(string2, '\');
        cd(strcat(string3, data_name_i));
    end
     if i>=10
        ii=num2str(i);
        data_name_i = data_name;
        data_name_i(end-2) = '0';
        data_name_i(end-1) = ii(1);
        data_name_i(end) = ii(2);
    
        string3 = strcat(string2, '\');
        cd(strcat(string3, data_name_i));
     end
     
        [X,map] = imread('luminescent.tif');
        if ~isempty(map)
            Im = ind2rgb(X,map);
        end
        
        figure(); hold on;
        imagesc(X);
        title(strcat('source',num2str(i)));         
        
        for ii=1:cont
            plot(x(ii),y(ii),'wx', 'LineWidth', 3,'MarkerSize', 10);
        end
            plot(x(i),y(i),'rx', 'LineWidth', 3,'MarkerSize', 10);
end
        






