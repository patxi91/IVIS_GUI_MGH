function varargout = IVIS(varargin)
% IVIS MATLAB code for IVIS.fig
%      IVIS, by itself, creates a new IVIS or raises the existing
%      singleton*.
%
%      H = IVIS returns the handle to a new IVIS or the handle to
%      the existing singleton*.
%
%      IVIS('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in IVIS.M with the given input arguments.
%
%      IVIS('Property','Value',...) creates a new IVIS or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before IVIS_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to IVIS_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help IVIS

% Last Modified by GUIDE v2.5 26-Feb-2018 11:02:08

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @IVIS_OpeningFcn, ...
                   'gui_OutputFcn',  @IVIS_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

% --- Executes just before IVIS is made visible.
function IVIS_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to IVIS (see VARARGIN)

% Choose default command line output for IVIS
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% This sets up the initial plot - only do when we are invisible
% so window can get raised using IVIS.
if strcmp(get(hObject,'Visible'),'off')
    imagesc(rand(250));
end

% UIWAIT makes IVIS wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = IVIS_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

% --- Executes on button press in browse.
function browse_Callback(hObject, eventdata, handles)
% hObject    handle to browse (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
axes(handles.axes1);
cla;

folder_path = uigetdir;
addpath(folder_path);
set(handles.datapath, 'string', folder_path);

cont=0;ref=0;
for i=size(folder_path,2):-1:1
    cont=cont+1;
    if folder_path(i) == '\'
        ref = cont;
        break
    end
end
name = folder_path(size(folder_path,2)-ref+2:size(folder_path,2));
handles.dataname.String = name;

path_this = pwd;
cd(folder_path);
av_data = ls;
cont = 0;
for i=1:size(av_data,1)
    if mean(av_data(i,1:3) =='PKI') && mean(av_data(i,end-3:end) ~='.PNG')
        cont = cont+1;
    end
end
handles.totalsources.String = cont;

% Retrieve X & Y source positions
for i = 1:cont
    
    data_name_i = handles.dataname.String;
    for j=3-size(num2str(i),2):-1:1 % maxiumum 999 sources
        data_name_i(end-3+j) = '0';
    end
    aux = num2str(i);
    for j=1:size(num2str(i),2)
        data_name_i(end-j+1) = aux(size(num2str(i),2)-j+1);
    end
    str_aux = strcat(folder_path, '\');
    cd(strcat(str_aux, data_name_i));

    fid  = fopen('ClickInfo.txt','r');
    text = textscan(fid,'%s','Delimiter','','endofline','');
    text = text{1}{1};
    fid  = fclose(fid);
    idx_x = strfind(text,'FL x-spot:');
    x(i) = round(str2num(text(min(idx_x)+11:min(idx_x)+18)));
    idx_y = strfind(text,'FL y-spot:');
    y(i) = round(str2num(text(min(idx_y)+11:min(idx_y)+18)));
    
end
for i = 1 % Plot always source#1 upfront
    
    data_name_i = handles.dataname.String;
    for j=3-size(num2str(i),2):-1:1 % maxiumum 999 sources
        data_name_i(end-3+j) = '0';
    end
    aux = num2str(i);
    for j=1:size(num2str(i),2)
        data_name_i(end-j+1) = aux(size(num2str(i),2)-j+1);
    end
    str_aux = strcat(folder_path, '\');
    cd(strcat(str_aux, data_name_i));
     
    [X,map] = imread('luminescent.tif');
    if ~isempty(map)
        Im = ind2rgb(X,map);
    end

    imagesc(X);hold on;   
    for ii=1:cont
        plot(x(ii),y(ii),'wx', 'LineWidth', 3,'MarkerSize', 10);
    end
        plot(x(i),y(i),'rx', 'LineWidth', 3,'MarkerSize', 10);
        hold off;
end

cd(path_this);
rmpath(folder_path);



% --------------------------------------------------------------------
function FileMenu_Callback(hObject, eventdata, handles)
% hObject    handle to FileMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% --------------------------------------------------------------------
function OpenMenuItem_Callback(hObject, eventdata, handles)
% hObject    handle to OpenMenuItem (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
file = uigetfile('*.fig');
if ~isequal(file, 0)
    open(file);
end

% --------------------------------------------------------------------
function PrintMenuItem_Callback(hObject, eventdata, handles)
% hObject    handle to PrintMenuItem (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
printdlg(handles.figure1)

% --------------------------------------------------------------------
function CloseMenuItem_Callback(hObject, eventdata, handles)
% hObject    handle to CloseMenuItem (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
selection = questdlg(['Close ' get(handles.figure1,'Name') '?'],...
                     ['Close ' get(handles.figure1,'Name') '...'],...
                     'Yes','No','Yes');
if strcmp(selection,'No')
    return;
end

delete(handles.figure1)

% --- Executes on selection change in popupmenu1.
function popupmenu1_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns popupmenu1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu1

% --- Executes during object creation, after setting all properties.
function popupmenu1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
     set(hObject,'BackgroundColor','white');
end

set(hObject, 'String', {'plot(rand(5))', 'plot(sin(1:0.01:25))', 'bar(1:.5:10)', 'plot(membrane)', 'surf(peaks)'});


% --- Executes on selection change in listbox1.
function listbox1_Callback(hObject, eventdata, handles)
% hObject    handle to listbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns listbox1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listbox1

totalsources = str2num(get(handles.totalsources, 'String'));
handles.listbox1.Min = 1;
handles.listbox1.Max = totalsources;

char_array = char(ones(totalsources,size(num2str(totalsources),2)) * ' ');
for i=1:totalsources
    aux = num2str(i);
    for j=1:size(num2str(i),2)
        char_array(i,j) = aux(j);
    end
end
handles.listbox1.String = char_array;

folder_path = handles.datapath.String;
cd(folder_path);
% Retrieve X & Y source positions
for i = 1:str2double(handles.totalsources.String)
    
    data_name_i = handles.dataname.String;
    for j=3-size(num2str(i),2):-1:1 % maxiumum 999 sources
        data_name_i(end-3+j) = '0';
    end
    aux = num2str(i);
    for j=1:size(num2str(i),2)
        data_name_i(end-j+1) = aux(size(num2str(i),2)-j+1);
    end
    str_aux = strcat(folder_path, '\');
    cd(strcat(str_aux, data_name_i));

    fid  = fopen('ClickInfo.txt','r');
    text = textscan(fid,'%s','Delimiter','','endofline','');
    text = text{1}{1};
    fid  = fclose(fid);
    idx_x = strfind(text,'FL x-spot:');
    x(i) = round(str2num(text(min(idx_x)+11:min(idx_x)+18)));
    idx_y = strfind(text,'FL y-spot:');
    y(i) = round(str2num(text(min(idx_y)+11:min(idx_y)+18)));
    
end
for i = handles.listbox1.Value % Plot for the selected source
    
    data_name_i = handles.dataname.String;
    for j=3-size(num2str(i),2):-1:1 % maxiumum 999 sources
        data_name_i(end-3+j) = '0';
    end
    aux = num2str(i);
    for j=1:size(num2str(i),2)
        data_name_i(end-j+1) = aux(size(num2str(i),2)-j+1);
    end
    str_aux = strcat(folder_path, '\');
    cd(strcat(str_aux, data_name_i));
     
    [X,map] = imread('luminescent.tif');
    if ~isempty(map)
        Im = ind2rgb(X,map);
    end

    imagesc(X);hold on;
    for ii=1:str2double(handles.totalsources.String)
        plot(x(ii),y(ii),'wx', 'LineWidth', 3,'MarkerSize', 10);
    end
        plot(x(i),y(i),'rx', 'LineWidth', 3,'MarkerSize', 10);
        hold off;
end
cd(folder_path);
cd('../');


% --- Executes during object creation, after setting all properties.
function listbox1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
