function [ imT ] = imR( img, angle)
%imR is a function that rotates a given "img" by "angle" degrees
%counter-clockwise.

% Method 1
imT = imrotate(img,angle,'bilinear','crop');

% % Method 2
% tform = affine2d([cosd(angle) -sind(angle) 0; sind(angle) cosd(angle) 0; 0 0 1]);
% imT = imwarp(img,tform);

end

