addpath('\\coresrv03.nmr.mgh.harvard.edu\opfluor\users\patxi\IVIS_GUI\coregistration\TSRH_transforms\IVIS_coregistration\PKI20180213124708_SEQ\XML\MultiModalityResults');
%addpath('/autofs/cluster\opfluor\users\patxi\IVIS_GUI\corregistration\TSRH_transforms\IVIS_corregistration\PKI20180213124708_SEQ\XML\MultiModalityResults');

tree = xmlread('MULTIMODALITY-20180213-161713.xml');
theStruct = parseXML('MULTIMODALITY-20180213-161713.xml');

matdim = str2double(theStruct(2).Children(4).Children(10).Attributes.Value);
tform = zeros(sqrt(matdim));% init

% Fill by Rows
tform_row = tform;
i=1;j=1;
for ii=1:size(theStruct(2).Children(4).Children(10).Children,2)
    if strcmp(theStruct(2).Children(4).Children(10).Children(ii).Name,'LI.MultiModality.Matrix')
        tform_row(i,j) = str2double(theStruct(2).Children(4).Children(10).Children(ii).Attributes.Value);
        j=j+1;
        if j > sqrt(matdim)
            i=i+1; j=1;
        end
    end
end

% Fill by Columns
tform_col = tform;
i=1;j=1;
for ii=1:size(theStruct(2).Children(4).Children(10).Children,2)
    if strcmp(theStruct(2).Children(4).Children(10).Children(ii).Name,'LI.MultiModality.Matrix')
        tform_col(i,j) = str2double(theStruct(2).Children(4).Children(10).Children(ii).Attributes.Value);
        i=i+1;
% Fill by Rows
tform_row = tform;
i=1;j=1;
for ii=1:size(theStruct(2).Children(4).Children(10).Children,2)
    if strcmp(theStruct(2).Children(4).Children(10).Children(ii).Name,'LI.MultiModality.Matrix')
        tform_row(i,j) = str2double(theStruct(2).Children(4).Children(10).Children(ii).Attributes.Value);
        j=j+1;
        if j > sqrt(matdim)
            i=i+1; j=1;
        end
    end
end

% Fill by Columns
tform_col = tform;
i=1;j=1;
for ii=1:size(theStruct(2).Children(4).Children(10).Children,2)
    if strcmp(theStruct(2).Children(4).Children(10).Children(ii).Name,'LI.MultiModality.Matrix')
        tform_col(i,j) = str2double(theStruct(2).Children(4).Children(10).Children(ii).Attributes.Value);
        i=i+1;
        if i > 4
            j=j+1; i=1;
        end
    end
end



        if i > 4
            j=j+1; i=1;
        end
    end
end


