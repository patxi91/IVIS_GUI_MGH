%%
% Load Images
X = dicomread('\\coresrv03.nmr.mgh.harvard.edu\opfluor\users\patxi\IVIS_GUI\coregistration\TSRH_transforms\IVIS_coregistration\PKI20180213124708_SEQ\CT Data\PKI20180213124708.dcm');
imread('\\coresrv03.nmr.mgh.harvard.edu\opfluor\users\patxi\IVIS_GUI\coregistration\TSRH_transforms\IVIS_coregistration\PKI20180213124708_SEQ\PKI20180213124708_001\luminescent.TIF');
luminescent = ans;

% Read XML
xml_main;

% Select tform
tform = tform_row;

% Manual Rotation and Scaling

img = squeeze(max((X),[],1));
CT_R = imrotate(img,90,'bilinear','crop'); % CT rotated

lum_S = imresize(luminescent,[2400 2400]); % luminescent scaled
CT_RS = imresize(CT_R,[2400 2400]);


%% Corregistration

%[Opos(1), Opos(2), Opos(3)] = transformPointsInverse(affine3d(tform4x4),Tpos(1), Tpos(2), Tpos(3));
% plot(Opos(1), Opos(2), 'r*', 'LineWidth', 2, 'MarkerSize', 15);

figure;
subplot(1,2,1);
imagesc(lum_S);
% imagesc(luminescent);
subplot(1,2,2);
imagesc(CT_RS);
while 1
    subplot(1,2,2);
    p = impoint;
    Tpos = getPosition(p);
    
    subplot(1,2,2);
    imagesc(CT_RS);
    hold on;
    plot(Tpos(1), Tpos(2), 'r*', 'LineWidth', 2, 'MarkerSize', 15);
    hold off;
    
    [Opos(1), Opos(2), Opos(3)] = transformPointsInverse(affine3d(tform_col),Tpos(1), Tpos(2), 300); % Depth 300 default
    subplot(1,2,1);
    imagesc(lum_S);
    hold on;
    plot(Opos(1), Opos(2), 'r*', 'LineWidth', 2, 'MarkerSize', 15);
    hold off;
end





