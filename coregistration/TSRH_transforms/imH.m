function [ imT ] = imH( img, varargin)
%imH is a function that shears a given input image "img" by a factor of "sx" in x and "sy" in y.

varargin = cell2mat(varargin);

if size(varargin,2) == 2
    sx = varargin(1);
    sy = varargin(2);
    tform = affine2d([1 sx 0; sy 1 0; 0 0 1]);
    imT = imwarp(img,tform);
end
if size(varargin,2) == 1
    sx = varargin;
    sy = sx;
    tform = affine2d([1 sx 0; sy 1 0; 0 0 1]);
    imT = imwarp(img,tform);
end
if size(varargin,2) ~= 2 && size(varargin,2) ~= 1
    error('Unexpected shear coefficients');
end

end

