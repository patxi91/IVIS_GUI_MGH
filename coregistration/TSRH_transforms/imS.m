function [ imT ] = imS( img, scale)
%imS is a function that scales a given "input_im" by a factor of "scale".

img = double(img);
scale = double(scale);
imT = imresize(img,scale);

end

