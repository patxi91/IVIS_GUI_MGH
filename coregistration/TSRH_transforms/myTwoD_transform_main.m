% This Script uses the 2D Affine Transformation to Translate, Scale, Shear
% and Rotate an image matrix.

%% Load image
I = phantom;

%% Transformation matrices

% Translation matrix
tx = 50;
ty = 25;
T = [1 0 0; 0 1 0; tx ty 1];

% Scale matrix
sx = 2;
sy = 3;
S = [sx 0 0; 0 sy 0; 0 0 1];

% Shear matrix
shx = 5;
shy = 1;
H = [1 shy 0; shx 1 0; 0 0 1];

% Rotation matrix
q = pi/4; % rad
R = [cos(q) sin(q) 0; -sin(q) cos(q) 0; 0 0 1];

%%

IT = T*I;
















