function varargout = coreg2D(varargin)
% COREG2D MATLAB code for coreg2D.fig
%      COREG2D, by itself, creates a new COREG2D or raises the existing
%      singleton*.
%
%      H = COREG2D returns the handle to a new COREG2D or the handle to
%      the existing singleton*.
%
%      COREG2D('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in COREG2D.M with the given input arguments.
%
%      COREG2D('Property','Value',...) creates a new COREG2D or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before coreg2D_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to coreg2D_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help coreg2D

% Last Modified by GUIDE v2.5 19-Mar-2018 13:32:11

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @coreg2D_OpeningFcn, ...
                   'gui_OutputFcn',  @coreg2D_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before coreg2D is made visible.
function coreg2D_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to coreg2D (see VARARGIN)

% Choose default command line output for coreg2D
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes coreg2D wait for user response (see UIRESUME)
% uiwait(handles.fig_mouse);


% --- Outputs from this function are returned to the command line.
function varargout = coreg2D_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



function edit3_Callback(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit3 as text
%        str2double(get(hObject,'String')) returns contents of edit3 as a double


% --- Executes during object creation, after setting all properties.
function edit3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit4_Callback(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit4 as text
%        str2double(get(hObject,'String')) returns contents of edit4 as a double


% --- Executes during object creation, after setting all properties.
function edit4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit5_Callback(hObject, eventdata, handles)
% hObject    handle to edit5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit5 as text
%        str2double(get(hObject,'String')) returns contents of edit5 as a double


% --- Executes during object creation, after setting all properties.
function edit5_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit6_Callback(hObject, eventdata, handles)
% hObject    handle to edit6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit6 as text
%        str2double(get(hObject,'String')) returns contents of edit6 as a double


% --- Executes during object creation, after setting all properties.
function edit6_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in apply.
function apply_Callback(hObject, eventdata, handles)
% hObject    handle to apply (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
I = loadf(handles);

if handles.cb1.Value == 1
    imT = imS( I, str2num(handles.edit3.String));
    tform = [str2num(handles.edit3.String) 0 0; 0 str2num(handles.edit3.String) 0; 0 0 1];
    axes(handles.axes3);
    imagesc(imT);
end

if handles.cb2.Value == 1
    eval('exist imT')
    eval('aux = ans');
    clc
    if aux == 1
        imT = imR( imT, str2num(handles.edit4.String));
%         tform = tform*[cosd(str2num(handles.edit4.String)) -sind(str2num(handles.edit4.String)) 0; sind(str2num(handles.edit4.String)) cosd(str2num(handles.edit4.String)) 0; 0 0 1];
        axes(handles.axes3);
        imagesc(imT);
    else
        imT = imR( I, str2num(handles.edit4.String));
%         tform = [cosd(str2num(handles.edit4.String)) -sind(str2num(handles.edit4.String)) 0; sind(str2num(handles.edit4.String)) cosd(str2num(handles.edit4.String)) 0; 0 0 1];
        axes(handles.axes3);
        imagesc(imT);
    end
end

if handles.cb3.Value == 1
    eval('exist imT')
    eval('aux = ans');
    clc
    if aux == 1
        imT = imH( imT, str2num(handles.edit5.String), str2num(handles.edit6.String));
        tform = tform*[1 str2num(handles.edit5.String) 0; str2num(handles.edit6.String) 1 0; 0 0 1];
        axes(handles.axes3);
        imagesc(imT);
    else
        imT = imH( I, str2num(handles.edit5.String), str2num(handles.edit6.String));
        tform = [1 str2num(handles.edit5.String) 0; str2num(handles.edit6.String) 1 0; 0 0 1];
        axes(handles.axes3);
        imagesc(imT);
    end
end

if handles.cb1.Value == 0 && handles.cb2.Value == 0 && handles.cb1.Value == 0
    handles.edit3.String = '1';
    imT = imS( I, 1);
    tform = [1 0 0; 0 1 0; 0 0 1];
    axes(handles.axes3);
    imagesc(imT);
end

% Mouse position on Transform Plot
while ishghandle(handles.axes3)
    axes(handles.axes3);
    p = impoint;
    Tpos = getPosition(p);
    Tpos_plot = Tpos;
    % Function to transform to original coordinates and assign values
    handles.cb1.Value = 1;
    if handles.cb2.Value == 1
        s=size(imT);
        marker=zeros(s(1:2));
        marker(floor(Tpos(2)),floor(Tpos(1)))=1;
        marker_rot = imrotate(marker,-str2num(handles.edit4.String),'bilinear','crop');
        if handles.cb1.Value == 0 && handles.cb3.Value == 0
            Opos(1) = round(find(marker_rot,1)/size(marker_rot,2));
            Opos(2) = find(marker_rot,1)-((Opos(1) - 1)*size(marker_rot,2))-1;
%             Opos(2) = abs( (size(marker_rot,1)*Opos(1)) - find(marker_rot,1));
        else            
            Tpos(1) = round(find(marker_rot,1)/size(marker_rot,2));
            Tpos(2) = find(marker_rot,1)-((Tpos(1) - 1)*size(marker_rot,2))-1;
            if Tpos(2) > size(marker_rot,2)
                Tpos(2) = Tpos(2)-size(marker_rot,2);
            end
        end
    end
    
    if handles.cb1.Value == 1 || handles.cb3.Value == 1% && handles.cb2.Value == 0
    [Opos(1), Opos(2)] = transformPointsInverse(affine2d(tform),Tpos(1),Tpos(2));
    end
    
    Tpos_plot    
    Tpos
    Opos
    handles.edit9.String = num2str(Tpos(1));
    handles.edit10.String = num2str(Tpos(2));
    handles.edit7.String = num2str(Opos(1));
    handles.edit8.String = num2str(Opos(2));
    imagesc(imT);
    hold on;
    plot(Tpos_plot(1), Tpos_plot(2), 'r*', 'LineWidth', 2, 'MarkerSize', 15);
    hold off;
    axes(handles.axes2);
    imagesc(I);
    hold on; 
    plot(Opos(1), Opos(2), 'r*', 'LineWidth', 2, 'MarkerSize', 15);
    hold off;
    
    if handles.cb1.Value == 0 && handles.cb2.Value == 0 && handles.cb1.Value == 0
        handles.cb1.Value = 0;
    end
end


% --- Executes on button press in load.
function load_Callback(hObject, eventdata, handles)
% hObject    handle to load (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
I = loadf(handles); % User function Below
axes(handles.axes2);
imagesc(I);

function edit7_Callback(hObject, eventdata, handles)
% hObject    handle to edit7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit7 as text
%        str2double(get(hObject,'String')) returns contents of edit7 as a double


% --- Executes during object creation, after setting all properties.
function edit7_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit8_Callback(hObject, eventdata, handles)
% hObject    handle to edit8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit8 as text
%        str2double(get(hObject,'String')) returns contents of edit8 as a double


% --- Executes during object creation, after setting all properties.
function edit8_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit9_Callback(hObject, eventdata, handles)
% hObject    handle to edit9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit9 as text
%        str2double(get(hObject,'String')) returns contents of edit9 as a double


% --- Executes during object creation, after setting all properties.
function edit9_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit10_Callback(hObject, eventdata, handles)
% hObject    handle to edit10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit10 as text
%        str2double(get(hObject,'String')) returns contents of edit10 as a double


% --- Executes during object creation, after setting all properties.
function edit10_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in cb1.
function cb1_Callback(hObject, eventdata, handles)
% hObject    handle to cb1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of cb1


% --- Executes on button press in cb2.
function cb2_Callback(hObject, eventdata, handles)
% hObject    handle to cb2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of cb2


% --- Executes on button press in cb3.
function cb3_Callback(hObject, eventdata, handles)
% hObject    handle to cb3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of cb3


% --- Executes on mouse motion over figure - except title and menu.
function fig_mouse_WindowButtonMotionFcn(hObject, eventdata, handles)
% hObject    handle to fig_mouse (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% 
% % To create this function double click in the gui background, change tag,
% % activate WindowButtonMotionFcn and copy paste code from http://we15hang.blogspot.com/2012/01/matlab-gui-tracking-mouse-locations.html
% 
% pos = get(hObject, 'currentpoint'); % get mouse location on figure
% x = pos(1); y = pos(2); % assign locations to x and y
% set(handles.lbl_x, 'string', ['x loc:' num2str(x)]); % update text for x loc
% set(handles.lbl_y, 'string', ['y loc:' num2str(y)]); % update text for y loc



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% User Functions Below                              %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function I = loadf(handles)
eval('exist I')
eval('auxI = ans');
eval('exist phantom')
eval('auxp = ans');
clc
if auxI == 1
    if length(size(I))~=2
        error('I data is not 2D');
        return
    end
end
if auxI == 0 && auxp == 2
    load('phantom.mat');
else
    if auxI == 0 && auxp == 0
    error('No existing data to plot');
    return
    end
end