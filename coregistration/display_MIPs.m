function display_MIPs( ReconBP )
%DISPLAY_MIPS Summary of this function goes here
%   Detailed explanation goes here

Recon_MIP = max((ReconBP),[],3);
Recon_MIP1 = squeeze(max((ReconBP),[],1));
Recon_MIP2 = squeeze(max((ReconBP),[],2));
Recon_MIP3 = squeeze(max((ReconBP),[],3));

% figure();
subplot(2,2,1); imagesc(Recon_MIP3); colormap('hot'); colorbar
subplot(2,2,2); imagesc(Recon_MIP2); colormap('hot'); colorbar
subplot(2,2,3); imagesc(Recon_MIP1.'); colormap('hot'); colorbar


end

