%Question No:7
%IMAGE ROTATION

function rotation(f)
[r,c]=size(f);
theta=input('Enter the rotaion angle = ');
rad=theta*(pi/180);
xc=floor(r/2);
yc=floor(c/2);
for i=1:r
    p=i-xc;
    for j=1:c
        q=j-yc;
        l=p*cos(rad)-q*sin(rad);
        m=p*sin(rad)+q*cos(rad);
        l=l+xc+r/2;
        m=m+yc+c/2;
        l=round(l);
        m=round(m);
        g(l,m)=f(i,j);
        g(l,m-1)=f(i,j);
        g(l,m+1)=f(i,j);
        g(l-1,m)=f(i,j);
        g(l+1,m)=f(i,j);
    end
end
imshow(f), figure, imshow(g);

